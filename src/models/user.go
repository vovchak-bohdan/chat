package models

import (
	"github.com/jinzhu/gorm"
)

type (
	User struct {
		gorm.Model
		AuthKey      string `gorm:"size:255;unique;not null" json:"auth_key,omitempty"`
		FingerPrint  string `gorm:"size:255;not null" json:"fingerprint,omitempty"`
		UserName     string `gorm:"size:255;unique" json:"user_name,omitempty"`
		PasswordHash string `gorm:"size:255" json:"-"`
	}
)
