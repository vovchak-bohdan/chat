package schemas

import (
	"bitbucket.org/vovchak-bohdan/chat/src/components/c_graphql"
	"bitbucket.org/vovchak-bohdan/chat/src/schemas/users"
)

var Modules = []c_graphql.GraphQlModule{
	users.UserModule,
}
