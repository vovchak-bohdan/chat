package users

import (
	"errors"
	"fmt"
	"github.com/graphql-go/graphql"
	"github.com/valyala/fasthttp"
	"math/rand"
	"time"
	"bitbucket.org/vovchak-bohdan/chat/src/components/c_graphql"
	"bitbucket.org/vovchak-bohdan/chat/src/models/user"
)

// Product contains information about one product
type Product struct {
	ID    int64   `json:"id"`
	Name  string  `json:"name"`
	Info  string  `json:"info,omitempty"`
	Price float64 `json:"price"`
}

var products = []Product{
	{
		ID:    1,
		Name:  "Chicha Morada",
		Info:  "Chicha morada is a beverage originated in the Andean regions of Perú but is actually consumed at a national level (wiki)",
		Price: 7.99,
	},
	{
		ID:    2,
		Name:  "Chicha de jora",
		Info:  "Chicha de jora is a corn beer chicha prepared by germinating maize, extracting the malt sugars, boiling the wort, and fermenting it in large vessels (traditionally huge earthenware vats) for several days (wiki)",
		Price: 5.95,
	},
	{
		ID:    3,
		Name:  "Pisco",
		Info:  "Pisco is a colorless or yellowish-to-amber colored brandy produced in winemaking regions of Peru and Chile (wiki)",
		Price: 9.95,
	},
}

var productType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "User",
		Fields: graphql.Fields{
			"fingerprint": &graphql.Field{
				Type: graphql.NewNonNull(graphql.String),
			},
			"user_name": &graphql.Field{
				Type: graphql.String,
			},
			"auth_key": &graphql.Field{
				Type: graphql.String,
			},
		},
	},
)

var queryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			/* Get (read) single user by fingerprint
			   http://localhost:8080/user?query={user(fingerprint:<hash>,user_name:"user_name",password:"password","auth_key":"auth_key",is_login:true){user_name,auth_key,fingerprint}}
			*/
			"user": &graphql.Field{
				Type:        productType,
				Description: "Get user by fingerprint",
				Args: graphql.FieldConfigArgument{
					"fingerprint": &graphql.ArgumentConfig{
						Type:        graphql.NewNonNull(graphql.String),
						Description: "Use for soft auth",
					},
					"user_name": &graphql.ArgumentConfig{
						Type:        graphql.String,
						Description: "Use for hard auth",
					},
					"password": &graphql.ArgumentConfig{
						Type:        graphql.String,
						Description: "Use for hard auth",
					},
					"auth_key": &graphql.ArgumentConfig{
						Type:        graphql.String,
						Description: "Use for hard auth",
					},
					"is_login": &graphql.ArgumentConfig{
						Type:         graphql.Boolean,
						DefaultValue: false,
						Description:  "Can be use for 'login' or 'signup'",
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					fingerprint, _ := p.Args["fingerprint"].(string)
					userName, isUserName := p.Args["user_name"].(string)
					password, isPassword := p.Args["password"].(string)
					isLogin, _ := p.Args["is_login"].(bool)

					var user = &user.Model{
						FingerPrint: fingerprint,
						UserName:    userName,
					}

					if (isUserName && !isPassword) || (!isUserName && isPassword) {
						if !isPassword {
							return nil, errors.New("password is required")
						}
						if !isUserName {
							return nil, errors.New("user_name is required")
						}
					} else if isUserName && isPassword {
						notFoundErr := user.FindByUserNameAndPassword([]byte(userName), []byte(password))

						fmt.Println(string(userName), string(password))

						if isLogin && notFoundErr != nil {
							return nil, errors.New("email and/or password is invalid")
						} else if !isLogin && notFoundErr == nil {
							return nil, errors.New("account is already registered")
						}
					} else if notFoundErr := user.FindByFingerprint([]byte(fingerprint)); notFoundErr != nil && isLogin {
						return nil, errors.New("account not found")
					}

					return user, user.Save()
				},
			},
			/* Get (read) chat list of friend
			   http://localhost:8080/product?query={chat_list{id,name,info,price}}
			*/
			"list": &graphql.Field{
				Type:        graphql.NewList(productType),
				Description: "Get product list",
				Resolve: func(params graphql.ResolveParams) (interface{}, error) {
					return products, nil
				},
			},
		},
	})

var mutationType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Mutation",
	Fields: graphql.Fields{
		/* Create new product item
		http://localhost:8080/product?query=mutation+_{create(name:"Inca Kola",info:"Inca Kola is a soft drink that was created in Peru in 1935 by British immigrant Joseph Robinson Lindley using lemon verbena (wiki)",price:1.99){id,name,info,price}}
		*/
		"create": &graphql.Field{
			Type:        productType,
			Description: "Create new product",
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"info": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"price": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Float),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				rand.Seed(time.Now().UnixNano())
				product := Product{
					ID:    int64(rand.Intn(100000)), // generate random ID
					Name:  params.Args["name"].(string),
					Info:  params.Args["info"].(string),
					Price: params.Args["price"].(float64),
				}
				products = append(products, product)
				return product, nil
			},
		},

		/* Update product by id
		   http://localhost:8080/product?query=mutation+_{update(id:1,price:3.95){id,name,info,price}}
		*/
		"update": &graphql.Field{
			Type:        productType,
			Description: "Update product by id",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"info": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"price": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				id, _ := params.Args["id"].(int)
				name, nameOk := params.Args["name"].(string)
				info, infoOk := params.Args["info"].(string)
				price, priceOk := params.Args["price"].(float64)
				product := Product{}
				for i, p := range products {
					if int64(id) == p.ID {
						if nameOk {
							products[i].Name = name
						}
						if infoOk {
							products[i].Info = info
						}
						if priceOk {
							products[i].Price = price
						}
						product = products[i]
						break
					}
				}
				return product, nil
			},
		},

		/* Delete product by id
		   http://localhost:8080/product?query=mutation+_{delete(id:1){id,name,info,price}}
		*/
		"delete": &graphql.Field{
			Type:        productType,
			Description: "Delete product by id",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				id, _ := params.Args["id"].(int)
				product := Product{}
				for i, p := range products {
					if int64(id) == p.ID {
						product = products[i]
						// Remove from product list
						products = append(products[:i], products[i+1:]...)
					}
				}

				return product, nil
			},
		},
	},
})

var UserModule = c_graphql.GraphQlModule{
	QueryParam: []byte("query"),
	Path:       []byte("/user"),
	Schema: c_graphql.SchemaIgnoreError(graphql.SchemaConfig{
		Query:    queryType,
		Mutation: mutationType,
	}),
	Handler: func(ctx *fasthttp.RequestCtx, query string, schema graphql.Schema) *graphql.Result {
		return graphql.Do(graphql.Params{
			Schema:        schema,
			RequestString: query,
		})
	},
}
