package c_gorm

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func DB() *gorm.DB {
	db, _ := gorm.Open("postgres", "host=pgpool port=5432 user=postgres dbname=customdatabase password=adminpassword")

	return db
	//defer db.Close()
}
