package c_graphql

import "github.com/graphql-go/graphql"

func SchemaIgnoreError(config graphql.SchemaConfig) graphql.Schema {
	schema, _ := graphql.NewSchema(config)

	return schema
}
