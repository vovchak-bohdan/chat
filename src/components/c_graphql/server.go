package c_graphql

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/graphql-go/graphql"
	"github.com/valyala/fasthttp"
	"io"
)

type (
	GraphQlModule struct {
		QueryParam GraphQlQueryParam
		Path       GraphQlModulePath
		Schema     graphql.Schema
		Handler    graphQlModuleHandler
	}
	GraphQlModulePath    []byte
	GraphQlQueryParam    []byte
	graphQlModuleHandler func(ctx *fasthttp.RequestCtx, query string, schema graphql.Schema) *graphql.Result
)

func Init(modules []GraphQlModule, ctx *fasthttp.RequestCtx) {
	var reqPath = ctx.Path()

	for _, module := range modules {
		if bytes.Compare(module.Path, reqPath) == 0 {

			res := module.Handler(ctx, string(ctx.QueryArgs().PeekBytes(module.QueryParam)), module.Schema)

			if ctx.Err() != nil {
				return
			}

			if len(res.Errors) > 0 {
				ctx.Error(fmt.Sprintf("errors: %v", res.Errors), fasthttp.StatusBadRequest)
				return
			}

			ctx.SetStatusCode(fasthttp.StatusOK)

			switch tRes := res.Data.(type) {
			case nil:
				ctx.SetStatusCode(fasthttp.StatusNoContent)
			case []byte:
				ctx.SetBody(tRes)
			case string:
				ctx.SetBodyString(tRes)
			case fasthttp.StreamWriter:
				ctx.SetBodyStreamWriter(tRes)
			case struct {
				Stream io.Reader
				Size   int
			}:
				ctx.SetBodyStream(tRes.Stream, tRes.Size)
			default:
				bts, err := json.Marshal(tRes)

				if err != nil {
					ctx.SetStatusCode(fasthttp.StatusBadGateway)
				} else {
					ctx.SetBody(bts)
				}
			}

			return
		}
	}
}