module bitbucket.org/vovchak-bohdan/chat

go 1.13

require (
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/dgraph-io/badger v1.6.0 // indirect
	github.com/dgraph-io/badger/v2 v2.0.1-rc1 // indirect
	github.com/graphql-go/graphql v0.7.8
	github.com/jinzhu/gorm v1.9.11
	github.com/valyala/fasthttp v1.7.0
	golang.org/x/crypto v0.0.0-20191219195013-becbf705a915 // indirect
)
