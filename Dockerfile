FROM golang:latest

WORKDIR /go/src/bitbucket.org/vovchak-bohdan/chat

RUN go get -u github.com/pilu/fresh

COPY go.mod .
COPY go.sum .
RUN go list -m all

COPY . .
RUN go mod vendor

EXPOSE 5000

ENTRYPOINT ["/go/bin/fresh"]